## Requirements
***
<ul>
    <li>Docker</li>
    <li>docker-compose</li>
    <li>Git (optionnel)</li>
</ul>

## Installation
***
<ul>
    <li>Cloner ou télécharger le repository</li>
    <li>Se rendre dans le dossier Deployment du repository</li>
    <li>Lancer <code>docker-compose up --build</code> dans un terminal</li>
    <li>Les 2 containers (application + API) se lancent</li>
    <li>Dans un navigateur, se rendre sur <code>&lt;IP-machine&gt;:5000</code> (IP-machine = localhost si execution sur la même machine)</li>
</ul>

## Architecture du repository
***
<ul>
    <li><strong>Dossier ApiModel :</strong> modèle utilisé pour l'API</li>
    <li><strong>Dossier App :</strong> fichiers de l'application</li>
    <li><strong>Dossier Data :</strong> contient les différents datasets selon le pré-traitement appliqué sur le jeu de base</li>
    <li><strong>Dossier Deployment :</strong> fichiers Docker pour le déploiement de l'application et de l'API</li>
    <li><strong>Dossier Models :</strong> dossiers des différents modèles sauvegardés lors du développpement de celui-ci</li>
</ul>