from flask import Flask, render_template, request, redirect, url_for

import tensorflow as tf
import numpy as np
import nsvision as nv
import grpc
from tensorflow_serving.apis.predict_pb2 import PredictRequest
from tensorflow_serving.apis import prediction_service_pb2_grpc

import os
import shutil
from werkzeug.utils import secure_filename
import random
import string

import utils

TMP_FOLDER = './static/tmp'
DATA_FOLDER = './static/data'

app = Flask(__name__)
app.config['TMP_FOLDER'] = TMP_FOLDER
app.config['DATA_FOLDER'] = DATA_FOLDER

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/start')
def start():
    return render_template('start.html')


@app.route('/prediction', methods=('GET', 'POST'))
def prediction():

    if request.method == 'GET':
        return redirect(url_for('start'))

    # Get image from request
    image = request.files['image']

    # Save image in tmp folder
    filename = secure_filename(image.filename)
    image.save(os.path.join(app.config['TMP_FOLDER'], filename))

    # Convert image into array
    image_array = utils.load_image(os.path.join(app.config['TMP_FOLDER'], filename), resize_mode="fill_pixel")
    image_array = np.expand_dims(image_array, axis=0)

    # Config of the gRPC request
    predict_request = PredictRequest()

    predict_request.model_spec.name = 'my_model'
    predict_request.model_spec.signature_name = "serving_default"

    input_name = 'sequential_2_input'
    predict_request.inputs[input_name].CopyFrom(tf.make_tensor_proto(image_array))

    channel = grpc.insecure_channel(f"{os.environ.get('API_HOSTNAME')}:8500")
    predict_service = prediction_service_pb2_grpc.PredictionServiceStub(channel)

    # Retrieve request response
    response = predict_service.Predict(predict_request, timeout=10.0)

    output_name = 'dense_3'
    outputs_proto = response.outputs[output_name]

    y_proba = tf.make_ndarray(outputs_proto)

    labels = ["Amanita bisporigera", "Amanita muscaria", "Boletus edulis", "Cantharellus", "Omphalotus olearius", "Russula mariae"]

    print("### Probabilities:", y_proba.round(2)[0], "###")
    print("### Prediction:", np.argmax(y_proba.round(2), axis=1)[0], "###")

    predictions = list(zip(labels, y_proba.round(2)[0] * 100))
    predictions.sort(key=(lambda el: el[1]), reverse=True)

    return render_template('prediction.html', predictions=predictions[:4], image=filename)


@app.route('/correction', methods=('GET', 'POST'))
def correction():

    # Check if file as query param and if it exists, else return to start
    filename = request.args.get('image_name')
    if not filename or not os.path.isfile(os.path.join(app.config['TMP_FOLDER'], filename)):
        return redirect(url_for('start'))

    if request.method == 'GET':
        return render_template('correction.html')

    # If POST, get label and move file to data folder
    correct_label = request.form['class']
    correct_label_path = os.path.join(app.config['DATA_FOLDER'], correct_label)
    if not os.path.isdir(correct_label_path):
        os.makedirs(correct_label_path)

    if correct_label == 'other':
        correct_class = ' '.join(request.form['correct_class'].lower().split())
        ext = filename.split('.')[-1]
        new_filename = secure_filename('_'.join(correct_class.split(' ')) + '_' + ''.join(random.choices(string.ascii_lowercase + string.digits, k=15)) + '.' + ext)
        shutil.move(os.path.join(app.config['TMP_FOLDER'], filename), os.path.join(correct_label_path, new_filename))
    else:
        shutil.move(os.path.join(app.config['TMP_FOLDER'], filename), os.path.join(correct_label_path, filename))

    return redirect(url_for('start'))