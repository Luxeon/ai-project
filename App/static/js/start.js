let fileInput = document.querySelector('.image-input');
let fileButton = document.querySelector('.image-button');
let filePreviewContainer = document.querySelector('.image-preview-container');

fileButton.addEventListener('click', (e) => {
    e.preventDefault();
})

fileInput.addEventListener('change', (e) => {
    let fileReader = new FileReader();

    fileReader.addEventListener('load', (e) => {
        let filePreview = document.createElement('img');
        filePreview.src = e.target.result;

        filePreviewContainer.innerHTML = '';
        filePreviewContainer.removeAttribute("hidden");
        filePreviewContainer.classList.add("visible")

        // remove class in case of multiple file uploads
        filePreview.classList.remove('fill-width');
        filePreview.classList.remove('fill-height');

        // fill the image size
        filePreview.classList.add(filePreview.width > filePreview.height ? 'fill-width' : 'fill-height');

        filePreview.classList.add("image-preview");

        fileButton.classList.remove("first-upload");
        fileButton.classList.add("reset-upload");
        fileButton.innerHTML = "Upload another image";
        
        filePreviewContainer.append(filePreview);
    })

    fileReader.readAsDataURL(e.target.files[0]);
})

fileButton.addEventListener('click', () => {
    fileInput.click();
})